/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed1prgt10e10.ej2basico;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import org.ceedcv.ceed1prgt10e10.ej1sakila.Actor;
import org.ceedcv.ceed1prgt10e10.ej1sakila.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author paco
 */
public class Main2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        try {

            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Actor actor = new Actor();
            Date fecha = new Date();

            // Create
            actor.setActorId(4);
            actor.setFirstName("Juan");
            actor.setLastName("Garcia");
            actor.setLastUpdate(fecha);

            session.save(actor);
            session.getTransaction().commit();

            // Read
            session.beginTransaction();
            String hql = "from Actor";
            Query q = session.createQuery(hql);
            ArrayList alumnos = (ArrayList) q.list();
            session.getTransaction().commit();

            Iterator it = alumnos.iterator();
            while (it.hasNext()) {
                System.out.println(it.next());
            }

            // Update
            session.beginTransaction();
            actor.setLastName("Gomez");
            session.update(actor);
            session.getTransaction().commit();

            // Update
            session.beginTransaction();
            session.delete(actor);
            session.getTransaction().commit();

        } catch (HibernateException he) {
            he.printStackTrace();
        }

    }

}
